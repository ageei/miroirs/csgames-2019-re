# CS Games 2019 - Rétro-ingénierie (reverse engineering)

Fichiers fournis à l'épreuve de rétro-ingénierie des CS Games 2019.
Le `Dockerfile` fournissait des outils utiles. Le fichier
`portal_lock.gz` est le _firmware_ avec lequel les compétiteurs
devaient travailler.

## Flags

- Open the firmware (25 points)
- Register (100 points) -- Une fois l'enregistrement réussi, le binaire
  à exploiter contactait un serveur distant pour récupérer le _flag_.
- Very Mega Protect (200 points) -- Le _payload_ devait être envoyé au
  binaire directement sur le système _ARM_ présent durant l'épreuve pour
  obtenir le _flag_.
- Read All (75 points) -- Le _payload_ aurait dû être exécuté sur le
  serveur _ARM_ présent durant l'épreuve. En raison de difficultés
  techniques, une validation de l'exploitation était faite par
  l'organisateur.
- Read All Optimized (150 points)

## Description officielle

```
Get your disassemblers and debuggers ready ladies and gentlemen,
a device capable of opening portals to new dimensions has been
discovered and we need your help to uncover its mysteries! All
we know is that’s running an ARM processor. Also, some services
are running on it and some might even be exploitable! We extracted
the firmware from it but can’t continue without you. You will be
rewarded greatly for your hard work!

Participants : 2 People

Skills : Laptop, ARM disassembler/decompiler, Debugger, Scripting
language of your choice, Binwalk, QEMU. A docker image with all
the needed tools will be handed if you don’t have them.

Duration : 3 hours

Weight : 6 %
```
